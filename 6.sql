SELECT categories.id AS id, categories.name AS category_name, group_concat(products.name separator ", ") AS products 
	FROM products,categories 
	WHERE categories.id = products.category_id 
	GROUP BY products.category_id