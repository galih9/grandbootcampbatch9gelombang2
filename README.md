# Arkademy code your future
jawaban dari tipe soal C batch 9 gelombang/kloter 2
- terdiri dari file jawaban dan diberi nomor sesuai dengan soal
- file jawaban 1 sampai 5 berformat php
- file jawaban nomor 6 berformat sql
- jawaban nomor 7 disatukan dalam satu folder
## Hal yang diperlukan agar program dapat berjalan
- Php Server
- Browser
## Langkah langkah penginstalan
- Jalankan server apache dan mysql
- copy project nya ke dalam folder htdocs
- import database ke server mysql
## Pengerjaan
- Menggunakan sublime text
- Xampp
- Browser firefox
## Informasi tambahan
- Menggunakan Php versi 5.3.36
- Menggunakan control panel versi 3.2.2