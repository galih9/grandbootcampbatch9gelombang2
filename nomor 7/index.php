<html>
  <head>
    <title>Nomor 7</title>
    <!-- Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- custom css -->
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <div class="container-fluid">
      <h2>Data Gudang</h2>
      <p>Aplikasi sederhana ini dibuat untuk memenuhi persyaratan grandbootcamp Arkademy dalam soal tipe C Batch 9 nomor 7 berdasarkan query soal nomor 6 </p>

      <?php
      $konek=mysqli_connect('localhost','root','','DBgudang');
      $sql = "SELECT categories.id AS id, categories.name AS category_name, group_concat(products.name separator ', ') AS products FROM products,categories WHERE categories.id = products.category_id GROUP BY categories.id;";
        $result=mysqli_query($konek, $sql);
        
        if (mysqli_num_rows($result) > 0) {
            ?>
            <table>
                <tr>
                  <th width="10px">id</th>
                  <th width="220px">Category Name</th>
                  <th >Product</th>
                </tr>
            <?php
            while($row = mysqli_fetch_assoc($result)) {
              ?>
                <tr>
                  <td><?php echo $row["id"]; ?></td>
                  <td><?php echo $row["category_name"]; ?></td>
                  <td><?php echo $row["products"]; ?></td>
                </tr>
              <?php
            }
        } else {
            echo "data kosong atau database belum/gagal di import";
        }
      ?>
      </table>      
      <h2>Tambah category</h2>
        <table>
          <tr>
            <td>Nama category :</td>
            <form action="nomor1.php" method="POST">
            <td><input type="text" name="name"></input></td>
            <td><input type="submit" value="CREATE !"></input></td>
            </form>
          </tr>
      <?php
      /*
      untuk melihat adanya perubahan maka harus menggunakan right / left join
      saya hanya menggunakan query sql dari nomor 6 saja jadi jika ingin melihat perubahan anda harus melihatnya sendiri di phpMyadmin atau cPanel
      */
        $name=$_POST['name'];
        if ($name) {
          $sql="INSERT INTO categories (name) VALUES ('$name');";
          if (mysqli_query($konek, $sql)) {
              echo "<p>Record baru telah dibuat</p>";
          } else {
              echo "Error: ".$sql."<br>".mysqli_error($konek);
          }
        }
        
      ?>
    </div>
  </body>
</html>