<?php
function HitungKembalian($harga, $uang) {
    $duapuluh=0;
    $sepuluh=0;
    $duaribu=0;
    $lima=0;
    
    $kembalian = $uang - $harga;
    while($kembalian!=0){
        if ($kembalian>=20000) {
            $kembalian = $kembalian - 20000;
            $duapuluh = $duapuluh +1;
        }elseif ($kembalian>=10000) {
            $kembalian = $kembalian - 10000;
            $sepuluh = $sepuluh + 1;
        }
        elseif ($kembalian>=2000) {
            $kembalian = $kembalian - 2000;
            $duaribu = $duaribu + 1;
        }
        elseif ($kembalian>=500) {
            $kembalian = $kembalian - 500;
            $lima = $lima + 1;
        }
    }
?>
<ul>
	<li>
	<?php
		if ($duapuluh!=0) {
        	echo $duapuluh;
     	}
	?>
	 x 20000</li>
	<li>
	<?php
		if ($sepuluh!=0) {
        	echo $sepuluh;
     	}
	?>
	 x 10000</li>
	<li>
	<?php
		if ($duaribu!=0) {
        	echo $duaribu;
     	}
	?>
	 x 2000</li>
	<li>
	<?php
		if ($lima!=0) {
        	echo $lima;
     	}
	?>
	x 500</li>
</ul>
<?php
}

//contoh
$uang=50000;
$harga=15500;
echo HitungKembalian($harga,$uang);

?>